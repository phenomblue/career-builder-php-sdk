<?php
/*
 * Plugin Name: Career Builder API
 * Plugin URI: http://www.phenomblue.com/
 * Description: Career Builder API wrapper
 * Author: Phenomblue
 * Version: 1.0.0
 * Author URI: http://www.phenomblue.com/
 *
 * -------------------------------------
 *
 * @package Career Builder API
 * @category Plugin
 * @author Jacob Dunn
 * @link http://www.phenomblue.com/ Phenomblue
 * @version 1.0.0
 *
 * -------------------------------------
 * 
 * For Further information, see http://developer.careerbuilder.com/endpoints/index
 *
 * -------------------------------------
 *
 * Career Builder API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* LESS EDITING BELOW */

define('CAREER_BUILDER_URL', 'https://api.careerbuilder.com/v2');
define('CAREER_BUILDER_VERSION', '1.0.0');
define('CAREER_BUILDER_FILE', __FILE__);
define('CAREER_BUILDER_BASENAME', plugin_basename(CAREER_BUILDER_FILE));
define('CAREER_BUILDER_PATH', plugin_dir_path(CAREER_BUILDER_FILE));

// Includes
foreach (glob(CAREER_BUILDER_PATH.'inc/*.php') as $filename)
	require_once($filename);

// Factories
foreach (glob(CAREER_BUILDER_PATH.'factories/*.php') as $filename)
	require_once($filename);

class CareerBuilder{

	const GET 		= 'GET';
	const POST 		= 'POST';
	const DELETE 	= 'DELETE';
	const PUT 		= 'PUT';

	public $defaultQuery; 
	public $useCache = true;
	public $cacheDuration = 10800; // 3 hours (60 * 60 * 3)
	public $retryAttempts = 3;

	public function CareerBuilder($query)
	{
		$this->defaultQuery = is_array($query)
			? $query
			: array();
	}

	public function invalidateCache()
	{
		global $wpdb;
		$wpdb->query(sprintf('DELETE FROM %s
			WHERE option_name LIKE \'%%cbapi-%%\'',
			$wpdb->options
			));
	}

	public function get($action,$data = false,$cacheDuration = false)
	{
		return $this->query($action,$data,self::GET,$cacheDuration = false);
	}
	
	public function post($action,$data = false,$cacheDuration = false)
	{
		return $this->query($action,$data,self::POST,$cacheDuration = false);
	}
	
	public function delete($action,$data = false,$cacheDuration = false)
	{
		return $this->query($action,$data,self::DELETE,$cacheDuration = false);
	}
	
	public function put($action,$data = false,$cacheDuration = false)
	{
		return $this->query($action,$data,self::PUT,$cacheDuration = false);
	}

	public function query($action,$data = false,$method = false,$cacheDuration = false)
	{
		if ($method === false) $method = self::GET;

		// Attempt our query until success, or we run out of retries
		$response = false;
		for ($attempt = 0; $attempt < $this->retryAttempts; $attempt++) { 
			$response = $this->remote($action,$data,array('method'=>$method),$cacheDuration = false);
			$response = $this->doFactoryForResponse($response,$action,$method);
			if(!$response->isError()) break;
		}

		return $response;
	}

	// Factory Methods for optional data parsing. Wrapped so we can redo if moved out of WordPress

	public static function registerFactory($factory,$action = false,$method = false,$priority = 10)
	{
		if ($factory instanceof ICareerBuilderFactory) {
			$path = array('');
			if($action !== false) $path []= $action;
			if($method !== false) $path []= strtolower($method);

			$path[0] = 'CB-API-request';
			add_filter(
				implode('/', $path),
				array(&$factory,'parseRequest'),
				$priority,
				3 );

			$path[0] = 'CB-API-response';
			add_filter(
				implode('/', $path),
				array(&$factory,'parseResponse'),
				$priority,
				3 );

			return true;
		}
		return false;
	}

	private function doFactoryForRequest($request,$action,$method)
	{
		$path = array(
			'CB-API-request',
			$action,
			strtolower($method)
			);
		$current = '';

		foreach ($path as $part) {
			$current .= $current == '' ? $part : '/'.$part;
			$request = apply_filters(
				$current,
				$request,
				$action,
				$method );
		}

		return $request;
	}

	private function doFactoryForResponse($response,$action,$method)
	{
		$path = array(
			'CB-API-response',
			$action,
			strtolower($method)
			);
		$current = '';

		foreach ($path as $part) {
			$current .= $current == '' ? $part : '/'.$part;
			$response = apply_filters(
				$current,
				$response,
				$action,
				$method );
		}

		return $response;
	}

	// Wrapping the remote mechanisms, so we can use other methods if not embedded in WordPress

	public function remote($action,$data = false,$args = false,$cacheDuration = false)
	{
		$baseURL = trim(CAREER_BUILDER_URL,'/');
		$actionURL = trim($action,'/');

		// Some APIs like to use URLs outside the normal structure. use ~/ to discard the regular path
		if(preg_match('#^~/#', $actionURL)){
			$parts = (object)parse_url($baseURL);
			$baseURL = str_replace($parts->path, '', $baseURL);
			$actionURL = preg_replace('#^~/#', '', $actionURL);
		}

		$url = sprintf('%1$s/%2$s',$baseURL,$actionURL);
		$args = wp_parse_args($args,array('method'=>self::GET));
		$queryData = wp_parse_args($args['method'] == self::GET ? $data : array(),$this->defaultQuery);

		foreach ($queryData as $key => $value) {
			// Booleans need to be true/false, not 1/0
			if($value === true) $queryData[$key] = 'true';
			if($value === false) $queryData[$key] = 'false';

			// Don't pass empty parameters
			if(empty($value)) unset($queryData[$key]);
		}

		// Some query arguments may need to be in the URL structure itself
		$filteredQueryData = $queryData;
		while (preg_match('/{(.*?)}/', $url, $match)) {
			$parameter = '';

			if(isset($filteredQueryData[$match[1]])){
				$parameter = $filteredQueryData[$match[1]];
				unset($filteredQueryData[$match[1]]);
			}

			$url = str_replace($match[0], $parameter, $url);
		}

		// Allow our factories to take a swing at the query
		$filtered = $this->doFactoryForRequest((object)array(
			'url'=>$url,
			'queryData'=>$filteredQueryData,
			'data'=>$data,
			),$action,$args['method']);

		$url = $filtered->url;
		$filteredQueryData = $filtered->queryData;
		$data = $filtered->data;

		// Arguments are passed differently based upon method
		switch ($args['method']) {
			case self::GET:
				if(count($filteredQueryData) > 0) $url .= sprintf('?%s',http_build_query($filteredQueryData));
				break;
			case self::POST:
			case self::DELETE:
			case self::PUT:
				if(count($filteredQueryData) > 0) $url .= sprintf('?%s',http_build_query($filteredQueryData));
				$args['body'] = $data;
				break;
			
			default:
				throw new WP_Error( 'http_request_failed', __('Invalid Method used for CareerBuilder::remote'));
				break;
		}

		// See if we have a cached response for GET requests
		$response = false;
		$key = sprintf('cbapi-%s',md5($url));
		if($args['method'] == self::GET && $this->useCache)
			$response = get_transient($key);

		// Make a remote request
		if(!$response || empty($response))
			$response = wp_remote_request( $url, $args );

		// Is this an error?
		if (is_wp_error($response)) return $response;
		
		// Cache responses for GET method requests, if OK
		if($args['method'] == self::GET && $response['response']['code'] == 200)
			set_transient($key,$response,$cacheDuration == false ? $this->cacheDuration : $cacheDuration);

		// Load in our XML
		libxml_use_internal_errors(true);
		libxml_clear_errors();

		// Parse into our response object
		$response = array(
			'xml'			=> simplexml_load_string($response['body']),
			'query_data'	=> $queryData,
			'query_url'		=> $url,
			'query_args'	=> $args,
			'body_data'		=> $args['method'] == self::PUT ? '' : $data
			);

		// Bad XML
		if(!isset($response['xml']) || empty($response['xml'])){
			$response['xml_errors'] = array();
			foreach (libxml_get_errors() as $error) {
				$response['xml_errors'] []= $error->message;
			}
		}

		return $response;
	}
}