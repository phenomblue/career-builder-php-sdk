<?php

class JobSearchFactory extends AbstractCareerBuilderFactory
{
	public function parseRequest($request,$action,$method)
	{
		$this->addEntity($request);
		$this->fixRadius($request);
		$this->fixCategory($request);

		return $request;
	}

	private function fixRadius($request)
	{
		// BUGFIX: City State searches should be condensed to just a location, if we want a radius
		if(	 empty($request->queryData['Location']) &&
			!empty($request->queryData['Radius']) &&
			(!empty($request->queryData['FacetCity']) || !empty($request->queryData['FacetState']))
			){

			$location = array();

			if(!empty($request->queryData['FacetCity'])) $location []= $request->queryData['FacetCity'];
			if(!empty($request->queryData['FacetState'])){

				// BUGFIX: If we had set a state, set the country code
				$reference = CareerBuilderConstants::getCountryReference();
				$key = strtolower($request->queryData['FacetState']);
				if(isset($reference[$key])) $request->queryData['CountryCode'] = $reference[$key];

				$location []= $request->queryData['FacetState'];
			}

			$request->queryData['Location'] = implode($location, ', ');

			unset($request->queryData['FacetCity']);
			unset($request->queryData['FacetState']);
		}
	}

	private function fixCategory($request)
	{
		// BUGFIX: Career Builder's API does not always respect category names - translate to codes
		if(isset($request->queryData['Category']) && !empty($request->queryData['Category'])){
			// Split the array, and see if each is a name in our Name->Code array
			$categories = explode(',', $request->queryData['Category']);
			$codes = $this->getCategoryCodes($request);

			foreach ($categories as &$category) {
				$category = trim($category);
				if(isset($codes[$category])) $category = $codes[$category];
			}

			// Now overwrite
			$request->queryData['Category'] = implode(',', $categories);
		}
	}

	private function addEntity($request)
	{
		// BUGFIX: Toggle SiteEntity for our search, they apparently can't add a default parameter when TalentNetwork is specified
		if(isset($request->queryData['TalentNetworkDID']) && !isset($request->queryData['SiteEntity']))
			$request->queryData['SiteEntity'] = 'talentnetworkjob';
	}

	private function getCategoryCodes($request)
	{
		// Get codes from the API
		$api = new CareerBuilder(array('DeveloperKey'=>$request->queryData['DeveloperKey']));
		$categories = $api->get('categories',false,604800); // cache for a week: 7 * 24 * 60 * 60

		if($categories->isError() || empty($categories->xml->Categories)) return array();

		// Return as a Name->Code array
		$simplified = array();
		$xml = $categories->xml->xpath('Categories');
		foreach ($xml[0] as $category)
			$simplified[(string)$category->Name] = (string)$category->Code;

		return $simplified;
	}
}

$parser = new JobSearchFactory();
CareerBuilder::registerFactory($parser,'jobsearch');