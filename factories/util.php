<?php
class UtilFactory extends AbstractCareerBuilderFactory
{
	public function parseResponse($response,$action,$method)
	{
		$response->addDecorator(new UtilDecorator($response));
		return $response;
	}
}

CareerBuilder::registerFactory(new UtilFactory());

// General methods
class UtilDecorator extends AbstractCareerBuilderDecorator
{

	public function extendResults($results,$path)
	{
		$base = $this->response->xml->xpath($path)[0];
		$extend = $results->xml->xpath($path)[0];

		foreach ($extend->children() as $child) 
		{
			$this->mergeXML($base, $child);
		}
	}

	public function xpath($path)
	{
		return $this->response->xml->xpath($path);
	}

	private function mergeXML(&$base, $add) 
	{ 
		$new = ( $add->count() != 0 )
			? $base->addChild($add->getName())
			: $base->addChild($add->getName(), $add);

		foreach ($add->attributes() as $a => $b) 
		{ 
			$new->addAttribute($a, $b); 
		}
		
		if ( $add->count() != 0 )
		{ 
			foreach ($add->children() as $child) 
			{ 
				$this->mergeXML($new, $child);
			} 
		} 
	}
}