<?php

// Jobs Parsing
class FeaturedPageFactory extends AbstractCareerBuilderFactory
{
	public function parseResponse($response,$action,$method)
	{
		$response->addDecorator(new FeaturedPageDecorator($response));
		return $response;
	}
}

class FeaturedPageDecorator extends AbstractCareerBuilderDecorator
{

	public function hasJobs()
	{
		return 	isset($this->response->xml->Response) &&
				isset($this->response->xml->Response->Results) &&
				isset($this->response->xml->Response->Results->JobSearchResult) &&
				count($this->response->xml->Response->Results->JobSearchResult->TNJobSearchResult) > 0;
	}

	public function jobsCount()
	{
		return $this->response->xml->Response->TotalCount;
	}

	public function jobs()
	{
		if(!$this->hasJobs()) return array();
		return $this->response->xml->Response->Results->JobSearchResult->TNJobSearchResult;
	}

	public function job()
	{
		if($this->hasJobs()){
			$jobs = $this->jobs();
			return $jobs[0];
		}
		return false;
	}
}

$parser = new FeaturedPageFactory();
CareerBuilder::registerFactory($parser,'~/talentnetwork/featuredpage/{FeaturedPageDID}/');