<?php

// Employee Type Parsing
class EmployeeTypeFactory extends AbstractCareerBuilderFactory
{	
	public function parseResponse($response,$action,$method)
	{
		$response->addDecorator(new EmployeeTypeDecorator($response));
		return $response;
	}
}

class EmployeeTypeDecorator extends AbstractCareerBuilderDecorator
{
	public function typeByCode($code)
	{
		if(!$this->hasTypes()) return false;

		return $this->response->xml->xpath(sprintf('/ResponseEmployeeTypes/EmployeeTypes/EmployeeType[Code="%1$s"]',$code));
	}
	public function typeByName($name)
	{
		if(!$this->hasTypes()) return false;

		return $this->response->xml->xpath(sprintf('/ResponseEmployeeTypes/EmployeeTypes/EmployeeType[Name="%1$s"]',$name));
	}

	public function hasTypes()
	{
		return isset($this->response->xml->EmployeeTypes);
	}
}

$parser = new EmployeeTypeFactory();
CareerBuilder::registerFactory($parser,'employeetypes');