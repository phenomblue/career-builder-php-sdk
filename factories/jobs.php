<?php

// Jobs Parsing
class JobsFactory extends AbstractCareerBuilderFactory
{
	public function parseResponse($response,$action,$method)
	{
		$response->addDecorator(new JobsDecorator($response));
		return $response;
	}
}

class JobsDecorator extends AbstractCareerBuilderDecorator
{
	public function isJob()
	{
		if($this->response->isError()) return false;
		return isset($this->response->xml->Job) || isset($this->response->xml->Response);
	}

	public function hasJobs()
	{
		if($this->response->isError()) return false;
		return 	isset($this->response->xml->Results) &&
				isset($this->response->xml->Results->JobSearchResult) &&
				count($this->response->xml->Results->JobSearchResult) > 0;
	}

	public function jobsCount()
	{
		if($this->response->isError()) return 0;
		return $this->response->xml->TotalCount;
	}

	public function jobs()
	{
		if(!$this->hasJobs()) return array();
		return $this->response->xml->Results->JobSearchResult;
	}

	public function job()
	{
		if($this->hasJobs()){
			$jobs = $this->jobs();
			return new Job($jobs[0]);
		}
		if(!$this->isJob()) return false;
		return new Job(isset($this->response->xml->Job) ? $this->response->xml->Job : $this->response->xml->Response);
	}
}

// Wrap single job postings with a class to standardize properties for Talent Network jobs vs Career Builder jobs
class Job{
	public $xml;

	private $keyLibrary = array(
		'JobTitle'=>array('Title'),
		'LocationCity'=>array('Location/CityName'),
		'LocationState'=>array('Location/StateName'),
		'DatePosted'=>array('ModifiedDate'),
		'EmploymentType'=>array('JobType'),
		'ExperienceRequired'=>array('Experience'),
		'DisplayJobID'=>array('ExternalKey'),
		'Details'=>array('JobDescription'),
		'Requirements'=>array('JobRequirements'),
		);

	private $properties = array();

	public function __set($property, $value)
	{
		$this->properties[$property] = $value;
	}

	public function __get($property) {
		if(!empty($this->properties[$property])) return $this->properties[$property];
		if(property_exists($this->xml, $property)) return (string)$this->xml->$property;

		if(isset($this->keyLibrary[$property]))
		foreach ($this->keyLibrary[$property] as $alt) {
			$value = $this->xml->xpath($alt);
			if(!empty($value)) return (string) $value[0];
		}

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        
        return null;
	}

	public function __isset($property) {
		if(!empty($this->properties[$property])) return true;
		if(property_exists($this->xml, $property)) return true;

		if(isset($this->keyLibrary[$property]))
		foreach ($this->keyLibrary[$property] as $alt) {
			$value = $this->xml->xpath($alt);
			if(!empty($value)) return true;
		}

		return false;
	}

	public function Job($xml)
	{
		$this->xml = $xml;
	}
}

$parser = new JobsFactory();
CareerBuilder::registerFactory($parser,'jobsearch');
CareerBuilder::registerFactory($parser,'job');
CareerBuilder::registerFactory($parser,'~/talentnetwork/job/{DID}');