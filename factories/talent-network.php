<?php

// Jobs Parsing
class TalentNetworkFactory implements ICareerBuilderFactory
{
	public function parseRequest($request,$action,$method)
	{
		// Create an array matching the XML structure
		$structure = array(
			'DeveloperKey'=>'',
			'TalentNetworkDID'=>'',
			'PreferredLanguage'=>'USEnglish',
			'AcceptPrivacy'=>'',
			'AcceptTerms'=>'',
			'ResumeWordDoc'=>''
			);

		// Get our defaults from the querydata or body data
		foreach ($structure as $key => $value) {
			if(!empty($request->queryData[$key])) $structure[$key] = $request->queryData[$key];
			if(!empty($request->data[$key])) $structure[$key] = $request->data[$key];
			unset($request->queryData[$key]);
			unset($request->data[$key]);

			// Remove any Empty Elements
			if(empty($structure[$key])) unset($structure[$key]);
		}

		// Now add all remaining join values
		$structure['JoinValues'] = array();
		foreach ($request->data as $key => $value) {
			if(empty($value)) continue;

			$structure['JoinValues'] []= array(
				'Key'=>$key,
				'Value'=>$value,
				);
		}

		// Convert to our XML request
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><Request></Request>');

		// function call to convert array to xml
		$this->array_to_xml($structure,$xml);

		$request->data = $xml->asXML();

		return $request;
	}
	
	public function parseResponse($response,$action,$method)
	{
		$response->addDecorator(new TalentNetworkDecorator($response));
		return $response;
	}

	private function array_to_xml($array,&$xml,$parentKey = '')
	{
		$parentKey = preg_replace('/s$/', '', $parentKey);

		foreach($array as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml->addChild("$key");
					$this->array_to_xml($value, $subnode, $key);
				}else if(!empty($parentKey)){
					$subnode = $xml->addChild("$parentKey");
					$this->array_to_xml($value, $subnode);
				}else{
					$subnode = $xml->addChild("item$key");
					$this->array_to_xml($value, $subnode);
				}
			}else{
				$xml->addChild("$key",htmlspecialchars("$value"));
			}
		}
	}
}

class TalentNetworkDecorator extends AbstractCareerBuilderDecorator
{
	public function formattedValidationErrors()
	{
		$formatted = array();
		foreach ($this->response->errors() as $error) {
			$formatted []= $this->formattedValidationError($error);
		}
		return $formatted;
	}

	public function formattedValidationError($error)
	{
		$formatted = '';
		switch ($error) {
			case 'EmptyEmail': 				$formatted = 'A valid email address is required.'; break;
			case 'EmailInUse': 				$formatted = 'That email address is already in use.'; break;
			case 'MemberExists': 			$formatted = 'That member already exists in our talent network.'; break;
			case 'PasswordMismatch': 		$formatted = 'The passwords provided do not match.'; break;
			case 'WeakPassword': 			$formatted = 'The password provided was too weak.'; break;
			case 'LongJobTypeInterest': 	$formatted = 'The Job Type Interest provided was too long.'; break;
			case 'LongInterestLevel': 		$formatted = 'The Job Interest provided was too long.'; break;
			case 'LongDesiredJobTitle': 	$formatted = 'The Job Title provided was too long.'; break;
			case 'LongZipCode': 			$formatted = 'The Zip Code provided was too long.'; break;
			case 'LongFirstName': 			$formatted = 'The First Name provided was too long.'; break;
			case 'LongLastName': 			$formatted = 'The Last Name provided was too long.'; break;
			case 'InvalidEmail': 			$formatted = 'The email address provided was invalid.'; break;
			case 'LongEmail': 				$formatted = 'The email address provided was too long.'; break;
			case 'InvalidCityState': 		$formatted = 'The email address provided was too long.'; break;
			case 'InvalidCityCountry': 		$formatted = 'The city provided was invalid.'; break;
			case 'InvalidPostalCountry': 	$formatted = 'The postal code provided was invalid.'; break;
			case 'EmptyCountry': 			$formatted = 'Please select a Country.'; break;
			case 'EmptyPostal': 			$formatted = 'Please enter a Postal Code.'; break;
			case 'LongPostal': 				$formatted = 'The Postal Code provided was too long.'; break;
			case 'LongCountry': 			$formatted = 'The Country provided was too long.'; break;
			case 'MissingResponse': 		$formatted = 'You are missing a required field.'; break;
			case 'CustomQuestionTooLong': 	$formatted = 'One of your responses was too long.'; break;
			case 'PersonalInfoResponse': 	$formatted = 'Personal information we are unable to store was detected in your response.'; break;
			case 'PersonalInfoResume': 		$formatted = 'Personal information we are unable to store was detected in your resume.'; break;
			case 'StoreFailed': 			$formatted = 'We failed to store your response. Please try again later.'; break;
			default: 						$formatted = 'An Unknown Error Occurred'; break;
		}

		return __($formatted);
	}
}

$parser = new TalentNetworkFactory();
CareerBuilder::registerFactory($parser,'~/talentnetwork/member/create');