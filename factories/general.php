<?php
class GeneralFactory extends AbstractCareerBuilderFactory
{	
	public function parseResponse($response,$action,$method)
	{
		$response->addDecorator(new GeneralDecorator($response));
		return $response;
	}
}

CareerBuilder::registerFactory(new GeneralFactory());

// General methods
class GeneralDecorator extends AbstractCareerBuilderDecorator
{
	public function GeneralDecorator($response){
		$this->response = $response;

		if(!$this->isError())
			$this->base = isset($this->response->xml->Response) ? $this->response->xml->Response : $this->response->xml;
	}

	public function errors()
	{
		if(isset($this->response->errors)) return $this->response->errors;
		if(isset($this->response->xml_errors)) return $this->response->xml_errors;
		if(isset($this->response->xml->Errors)) return $this->response->xml->Errors->children();
		if(!isset($this->response->xml)) return array('Invalid XML.');
		return array();
	}

	public function isError()
	{
		return count($this->errors()) > 0;
	}

	public function isPaged()
	{
		if($this->isError()) return false;
		return isset($this->base->TotalPages) && intval($this->base->TotalPages) > 1;
	}

	public function hasNextPage()
	{
		return ($this->isPaged() && intval($this->base->LastItemIndex) < intval($this->base->TotalCount));
	}

	public function hasPrevPage()
	{
		return ($this->isPaged() && intval($this->base->FirstItemIndex) > 0);
	}

	public function pageSize()
	{
		if(!$this->isPaged()) return 0;
		return isset($this->response->query_data['PerPage']) ? intval($this->response->query_data['PerPage']) : 25;
	}

	public function currentPage()
	{
		if(!$this->isPaged()) return 1;
		return isset($this->response->query_data['PageNumber']) ? intval($this->response->query_data['PageNumber']) : 1;
	}

	public function lastPage()
	{
		if(!$this->isPaged()) return 1;
		return $this->base->TotalPages;
	}

	public function nextPage()
	{
		if(!$this->isPaged() || !$this->hasNextPage()) return $this->lastPage();
		return $this->currentPage() + 1;
	}

	public function prevPage()
	{
		if(!$this->isPaged() || !$this->hasPrevPage()) return 1;
		return $this->currentPage() - 1;
	}

	public function nextPageQuery($api)
	{
		if(!$this->hasNextPage()) return false;

		$data = $this->response->query_data;
		$data['PageNumber'] = $this->currentPage() + 1;

		return $api->query($this->response->action,$data,$this->response->method);
	}
}