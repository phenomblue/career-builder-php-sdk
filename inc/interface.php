<?php

// The interface every factory implements
interface ICareerBuilderFactory{
	public function parseRequest($request,$action,$method);
	public function parseResponse($response,$action,$method);
}

class AbstractCareerBuilderFactory implements ICareerBuilderFactory
{
	public function parseRequest($request,$action,$method)
	{
		return $request;
	}
	
	public function parseResponse($response,$action,$method)
	{
		return $response;
	}
}