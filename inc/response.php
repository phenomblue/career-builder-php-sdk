<?php

// Our Top Level Response
class CareerBuilderResponseWrapper implements ICareerBuilderFactory
{
	public function parseRequest($request,$action,$method)
	{
		return $request;
	}

	public function parseResponse($response,$action,$method)
	{
		$response = new CareerBuilderResponse($response,$action,$method);
		return $response;
	}
}

CareerBuilder::registerFactory(new CareerBuilderResponseWrapper(),false,false,1);

// General response object.
class CareerBuilderResponse{

	protected $decorators = array();
	public function addDecorator($decorator) {
		$this->decorators[] = $decorator;
	}

	public function __call($method, $args) {
		foreach ($this->decorators as $decorator) {
			if(method_exists($decorator,$method))
				return call_user_func_array(array($decorator,$method), $args);
		}
	}

	public function CareerBuilderResponse($response,$action,$method)
	{
		$this->action = $action;
		$this->method = $method;

		if(is_array($response)){
			foreach ($response as $key => $value) {
				$this->$key = $value;
			}
		}else{
			$this->response = $response;
		}
	}
}