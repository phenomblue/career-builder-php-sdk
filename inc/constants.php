<?php

class CareerBuilderConstants{

	// Only certain distances are allowed: http://api.careerbuilder.com/Search/jobsearch/jobsearchinfo.aspx
	// 0 is the same as unlimited
	public static $distances = array(5, 10, 20, 30, 50, 100, 150);

	// Types of ordering allowed: http://api.careerbuilder.com/Search/jobsearch/jobsearchinfo.aspx
	// Relevance is the default
	public static $ordering = array('Relevance', 'Date', 'Pay', 'Title', 'Company', 'Distance', 'Location');

	public static $countryCodes = array(
		'us'=>'United States',
		'ca'=>'Canada'
		);

	public static $stateCodes = array(
		'us'=>array(
			'AL'=>'ALABAMA',
			'AK'=>'ALASKA',
			'AS'=>'AMERICAN SAMOA',
			'AZ'=>'ARIZONA',
			'AR'=>'ARKANSAS',
			'CA'=>'CALIFORNIA',
			'CO'=>'COLORADO',
			'CT'=>'CONNECTICUT',
			'DE'=>'DELAWARE',
			'DC'=>'DISTRICT OF COLUMBIA',
			'FM'=>'FEDERATED STATES OF MICRONESIA',
			'FL'=>'FLORIDA',
			'GA'=>'GEORGIA',
			'GU'=>'GUAM GU',
			'HI'=>'HAWAII',
			'ID'=>'IDAHO',
			'IL'=>'ILLINOIS',
			'IN'=>'INDIANA',
			'IA'=>'IOWA',
			'KS'=>'KANSAS',
			'KY'=>'KENTUCKY',
			'LA'=>'LOUISIANA',
			'ME'=>'MAINE',
			'MH'=>'MARSHALL ISLANDS',
			'MD'=>'MARYLAND',
			'MA'=>'MASSACHUSETTS',
			'MI'=>'MICHIGAN',
			'MN'=>'MINNESOTA',
			'MS'=>'MISSISSIPPI',
			'MO'=>'MISSOURI',
			'MT'=>'MONTANA',
			'NE'=>'NEBRASKA',
			'NV'=>'NEVADA',
			'NH'=>'NEW HAMPSHIRE',
			'NJ'=>'NEW JERSEY',
			'NM'=>'NEW MEXICO',
			'NY'=>'NEW YORK',
			'NC'=>'NORTH CAROLINA',
			'ND'=>'NORTH DAKOTA',
			'MP'=>'NORTHERN MARIANA ISLANDS',
			'OH'=>'OHIO',
			'OK'=>'OKLAHOMA',
			'OR'=>'OREGON',
			'PW'=>'PALAU',
			'PA'=>'PENNSYLVANIA',
			'PR'=>'PUERTO RICO',
			'RI'=>'RHODE ISLAND',
			'SC'=>'SOUTH CAROLINA',
			'SD'=>'SOUTH DAKOTA',
			'TN'=>'TENNESSEE',
			'TX'=>'TEXAS',
			'UT'=>'UTAH',
			'VT'=>'VERMONT',
			'VI'=>'VIRGIN ISLANDS',
			'VA'=>'VIRGINIA',
			'WA'=>'WASHINGTON',
			'WV'=>'WEST VIRGINIA',
			'WI'=>'WISCONSIN',
			'WY'=>'WYOMING',
			'AE'=>'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST',
			'AA'=>'ARMED FORCES AMERICA (EXCEPT CANADA)',
			'AP'=>'ARMED FORCES PACIFIC'
		),
		'ca'=>array(
			'Alberta'=>'Alberta',
			'British Columbia'=>'British Columbia',
			'Manitoba'=>'Manitoba',
			'New Brunswick'=>'New Brunswick',
			'Newfoundland and Labrador'=>'Newfoundland and Labrador',
			'Nova Scotia'=>'Nova Scotia',
			'Northwest Territories'=>'Northwest Territories',
			'Nunavut'=>'Nunavut',
			'Ontario'=>'Ontario',
			'Prince Edward Island'=>'Prince Edward Island',
			'Quebec	Québec'=>'Quebec	Québec',
			'Saskatchewan'=>'Saskatchewan',
			'Yukon'=>'Yukon'
		)
	);

	public static $countryReference;

	public static function getCountryReference()
	{
		if(empty(self::$countryReference)){

			self::$countryReference = array();
			foreach (self::$stateCodes as $country => $states) {
				foreach ($states as $abbreviation => $title) {
					self::$countryReference[strtolower($abbreviation)] = $country;
				}
			}
		}

		return self::$countryReference;
	}
}